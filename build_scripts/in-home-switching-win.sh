# Exit on non-0 exit codes
set -e

cd Windows/In-Home-Switching-Win

# https://github.com/NuGet/Home/issues/7341
mkdir nugettemp
sudo rm -rf /tmp/NuGetScratch/
ln -s /tmp/NuGetScratch/ $(pwd)/nugettemp/

mkdir packages
cd packages
nuget install SharpDX.Direct3D11
cd ..

msbuild
cp ../LICENSE* ./
cp ../README* ./
cp /opt/BUILTBY.txt ./

SHORT_GIT=$(echo $GIT_COMMIT | cut -c 1-7)
ZIP_NAME="$JOB_NAME-$SHORT_GIT.zip"

zip -9 -r $ZIP_NAME LICENSE* README* BUILTBY.txt In-Home-Switching/bin/Debug/*

cp $ZIP_NAME /var/www/bsnx/in-home-switching/
