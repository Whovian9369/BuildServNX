# Exit on non-0 exit codes
set -e

SHORT_GIT=$(echo $GIT_COMMIT | cut -c 1-7)
ZIP_NAME="$JOB_NAME-$SHORT_GIT.zip"
FOLDER_NAME="$JOB_NAME-out"

# requires installing cpio

make clean
rm -rf ./$FOLDER_NAME
mkdir $FOLDER_NAME

cp /opt/BUILTBY.txt ./$FOLDER_NAME
cp README* ./$FOLDER_NAME
cp LICENSE* ./$FOLDER_NAME

make -j3

find . -name '*.elf' | cpio -pdm ${FOLDER_NAME}
find . -name '*.kip' | cpio -pdm ${FOLDER_NAME}
find . -name '*.bin' | cpio -pdm ${FOLDER_NAME}
find . -name '*.nso' | cpio -pdm ${FOLDER_NAME}
find . -name '*.npdm' | cpio -pdm ${FOLDER_NAME}

cd $FOLDER_NAME
zip -9 -r $ZIP_NAME *

cp $ZIP_NAME /var/www/bsnx/$JOB_NAME/
