# Exit on non-0 exit codes
set -e

rm -rf ./*.zip
rm -rf ./BUILTBY.txt
rm -rf ./build

# requires installing cmake

cp /opt/BUILTBY.txt ./

mkdir build
cd build
cmake ..
make clean
make
SHORT_GIT=$(echo $GIT_COMMIT | cut -c 1-7)
ZIP_NAME="$JOB_NAME-$SHORT_GIT.zip"
cd ..

zip -9 -j $ZIP_NAME LICENSE* README* BUILTBY.txt bin/Release/nstool

cp $ZIP_NAME /var/www/bsnx/$JOB_NAME/
