# Exit on non-0 exit codes
set -e

make clean
rm -rf ./*.zip
rm -rf ./BUILTBY.txt

cp /opt/BUILTBY.txt ./

cd switch
DEVKITPRO="/opt/dkpstable" make -j3
cd ..
SHORT_GIT=$(echo $GIT_COMMIT | cut -c 1-7)
ZIP_NAME="$JOB_NAME-$SHORT_GIT.zip"

zip -9 -j $ZIP_NAME LICENSE* README* BUILTBY.txt switch/out/Checkpoint.*

cp $ZIP_NAME /var/www/bsnx/$JOB_NAME/ 
