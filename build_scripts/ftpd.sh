# Exit on non-0 exit codes
set -e

make clean
rm -rf ./*.zip
rm -rf ./BUILTBY.txt

cp /opt/BUILTBY.txt ./

make -f Makefile.switch
SHORT_GIT=$(echo $GIT_COMMIT | cut -c 1-7)
ZIP_NAME="ftpd-$SHORT_GIT.zip"

zip -9 -r $ZIP_NAME LICENSE* README* BUILTBY.txt ftpd.*

cp $ZIP_NAME /var/www/bsnx/$JOB_NAME/
