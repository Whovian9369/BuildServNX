# Exit on non-0 exit codes
set -e

make clean
rm -rf ./*.zip
rm -rf ./BUILTBY.txt
rm -rf /opt/devkitpro/libnx

cp /opt/BUILTBY.txt ./

make -j3
SHORT_GIT=$(echo $GIT_COMMIT | cut -c 1-7)
ZIP_NAME="libnx-$SHORT_GIT.zip"

make install
zip -r $ZIP_NAME LICENSE* README* BUILTBY.txt /opt/devkitpro/libnx

cp $ZIP_NAME /var/www/bsnx/libnx/
