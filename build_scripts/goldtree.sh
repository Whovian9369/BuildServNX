# Exit on non-0 exit codes
set -e

SHORT_GIT=$(echo $GIT_COMMIT | cut -c 1-7)
ZIP_NAME="$JOB_NAME-$SHORT_GIT.zip"

cd Goldtree

# https://github.com/NuGet/Home/issues/7341
mkdir nugettemp
sudo rm -rf /tmp/NuGetScratch/
ln -s /tmp/NuGetScratch/ $(pwd)/nugettemp/

mkdir packages
cd packages
nuget install LibHac
cd ..

msbuild
cp ../LICENSE* ./
cp ../README* ./
cp /opt/BUILTBY.txt ./

zip -9 -j -r $ZIP_NAME LICENSE* README* BUILTBY.txt Goldtree/bin/Debug/*

cp $ZIP_NAME /var/www/bsnx/goldleaf/

