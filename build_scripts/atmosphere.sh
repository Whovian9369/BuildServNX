# Exit on non-0 exit codes
set -e

SHORT_GIT=$(echo $GIT_COMMIT | cut -c 1-7)

# This is based on the Makefile inside atmosphere repo, the dist part
# However it also includes kips, exosphere and documentation

MAJORVER=$(grep '\ATMOSPHERE_RELEASE_VERSION_MAJOR\b' common/include/atmosphere/version.h | tr -s [:blank:] | cut -d' ' -f3)
MINORVER=$(grep '\ATMOSPHERE_RELEASE_VERSION_MINOR\b' common/include/atmosphere/version.h | tr -s [:blank:] | cut -d' ' -f3)
MICROVER=$(grep '\ATMOSPHERE_RELEASE_VERSION_MICRO\b' common/include/atmosphere/version.h | tr -s [:blank:] | cut -d' ' -f3)
AMSVER="$MAJORVER.$MINORVER.$MICROVER"

ZIP_NAME="$JOB_NAME-$AMSVER-$SHORT_GIT.zip"

git reset --hard
make clean

make all

mkdir -p out/sd/atmosphere/titles/0100000000000036
mkdir -p out/sd/atmosphere/titles/0100000000000034
mkdir -p out/sd/atmosphere/titles/0100000000000032/flags
cp fusee/fusee-primary/fusee-primary.bin out/fusee-primary-payload.bin
cp fusee/fusee-primary/fusee-primary.bin out/atmosphere/reboot_payload.bin
cp fusee/fusee-secondary/fusee-secondary.bin out/sd/atmosphere/fusee-secondary.bin
cp fusee/fusee-secondary/fusee-secondary.bin out/sd/sept/payload.bin
cp fusee/fusee-primary/fusee-primary.bin out/sd/atmosphere/reboot_payload.bin

cp sept/sept-primary/sept-primary.bin out/sd/sept/sept-primary.bin

# I don't have enough clout to have the right keys, so these will just be the latest sept files from latest release
cp /var/lib/sept/sept-secondary.bin out/sd/sept/sept-secondary.bin
cp /var/lib/sept/sept-secondary.enc out/sd/sept/sept-secondary.enc

cp common/defaults/BCT.ini out/sd/atmosphere/BCT.ini
cp common/defaults/loader.ini out/sd/atmosphere/loader.ini
cp common/defaults/system_settings.ini out/sd/atmosphere/system_settings.ini

cp -r common/defaults/kip_patches out/sd/atmosphere/kip_patches
cp stratosphere/creport/creport.nsp out/sd/atmosphere/titles/0100000000000036/exefs.nsp
cp stratosphere/fatal/fatal.nsp out/sd/atmosphere/titles/0100000000000034/exefs.nsp
cp stratosphere/eclct.stub/eclct.stub.nsp out/sd/atmosphere/titles/0100000000000032/exefs.nsp

cp troposphere/reboot_to_payload/reboot_to_payload.nro out/sd/switch/reboot_to_payload.nro

touch out/sd/atmosphere/titles/0100000000000032/flags/boot2.flag

mkdir -p out/kips/
cp stratosphere/sm/*.kip out/kips/
cp stratosphere/pm/*.kip out/kips/
cp stratosphere/ams_mitm/*.kip out/kips/
cp stratosphere/loader/*.kip out/kips/
cp exosphere/*.bin out/kips/  # okay, this isn't a kip but still, I can't think of a better folder for this 

cp /opt/BUILTBY.txt out/
cp README* out/
cp LICENSE* out/
cp -r docs/ out/docs/

cd out

zip -9 -r $ZIP_NAME *

cp $ZIP_NAME /var/www/bsnx/atmosphere/
