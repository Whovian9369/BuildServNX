# Exit on non-0 exit codes
set -e

cd Goldleaf

make clean

cp ../LICENSE* ./
cp ../README* ./
cp /opt/BUILTBY.txt ./

DEVKITPRO="/opt/dkpstable" make
SHORT_GIT=$(echo $GIT_COMMIT | cut -c 1-7)
ZIP_NAME="$JOB_NAME-$SHORT_GIT.zip"

zip -9 -j -r $ZIP_NAME LICENSE* README* BUILTBY.txt Output/*

cp $ZIP_NAME /var/www/bsnx/$JOB_NAME/

# call goldtree build after this

