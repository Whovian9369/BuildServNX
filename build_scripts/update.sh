# This script updates all packages and installs all switch packages automatically
# This is a major step in making BSNX fully-automated

# This script ignores errors to avoid leaving BSNX in a broken state
# Big thanks to https://support.cloudbees.com/hc/en-us/articles/218517228-How-to-Ignore-Failures-in-a-Shell-Step-

# Disable exit on non-0 exit codes
set +e

# Back up DKP
sudo mv /opt/devkitpro /opt/devkitproa || error=true
sudo mv /opt/dkpstable /opt/devkitpro || error=true

# Update packages
SYURESULT=$(sudo pacman -Syuq --noconfirm --force || error=true)
echo $SYURESULT | grep "jenkins" 2>&1 >/dev/null && echo do jenkins restart here 

# Install switch packages we don't have
SWITCHPACKAGES=$(pacman -Sl dkp-libs | grep 'switch' | awk '{print $2}' | tr '\n' ' ')
sudo pacman -S --needed $SWITCHPACKAGES --noconfirm --force || error=true

# Restore DKP
sudo mv /opt/devkitpro /opt/dkpstable || error=true
sudo mv /opt/devkitproa /opt/devkitpro || error=true

# Fail the build if there was an error
if [ $error ]
then 
    exit -1
fi
