# BuildServNX

```
   ____    ____     _   _      __  __   
U | __")u / __"| u | \ |"|     \ \/"/   
 \|  _ \/<\___ \/ <|  \| |>    /\  /\   
  | |_) | u___) | U| |\  |u   U /  \ u  
  |____/  |____/>> |_| \_|     /_/\_\   
 _|| \\_   )(  (__)||   \\,-.,-,>> \\_  
(__) (__) (__)     (_")  (_/  \_)  (__) 
```

BuildServNX (BSNX) is a Jenkins server configured to automatically build a large number of Nintendo Switch Console Modding related projects.

This repository includes the (sloppily written) build scripts and website assets related to it.

This is mostly here in case we have a drive failure (again) from the load BSNX gets.

## Links

You can find BSNX' build download site at https://bsnx.lavatech.top/

You can find BSNX' Jenkins instance at https://jenkins.lavatech.top/

## License

The scripts and website assets are released under GPLv2.

The programs these scripts build may be released under different licenses.
